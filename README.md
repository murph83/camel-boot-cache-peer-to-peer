Camel JCache example on Spring Boot
===================================

This application demonstrates configuring a peer-to-peer replicated cache for a Camel application deployed in Spring Boot. The route uses the JCache component, which allows for any API compliant caching impl to be chosen. For this example, I have chosen Infinispan. This is primarily driven by my experience with the technology, and the need for a dynamic, self-healing cluster. Infinispan accomplishes this with JGroups.

Using this example
------------------

In order to use this example, the IP address and port bindings for JGroups must be updated. This is configured in the jgroups.xml file under `src/resources`.

Specifically, set the `TCP.bind_addr`, `TCP.bind_port`, and `TCPPING.initial_hosts` properties to match your environment.
* `TCP.bind_addr` should be set to the network private address of the system. This address must be reachable from the other members of the peer network
* `TCP.bind_port` should be an open port on the host system. Traditionally, JGroups starts these ports at 7800 and goes up to 7801,7802,etc for multiple processes running on the same host
* `TCPPING.initial_hosts` should contain the address/port references for all of the members of the peer network. So if there are two peers running on two hosts `192.168.0.1` and `192.168.0.2`, both bound to port `7800`, the config would look like this:
    - Host 192.168.0.1

        ```xml
        <TCP bind_addr="192.168.0.1"
            bind_port="7800"
            ...
            />
     
        <TCPPING initial_hosts="192.168.0.1[7800],192.168.0.2[7800]
            ...
        ```

    - Host 192.168.0.2
    
        ```xml
        <TCP bind_addr="192.168.0.2"
             bind_port="7800"
             ...
             />
             
        <TCPPING initial_hosts="192.168.0.1[7800],192.168.0.2[7800]
            ...
        ```


